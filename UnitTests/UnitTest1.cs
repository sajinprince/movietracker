using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using MovieTracker.Services;
using NUnit.Framework;
using System;

namespace UnitTests
{
    public class Tests:Test2
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var builder = WebApplication.CreateBuilder();
            string connectionString = builder.Configuration.GetConnectionString("AuthConnectionString") ?? throw new InvalidOperationException("Connection string 'MovieTrackerContextConnection' not found.");
            var repo=new Repository(connectionString);
            Testme("test1");
            var service = new MovieTracker.Services.Tracking(repo);
            var res= service.GetSeriesList(1);
            Assert.True(res.Count > 0);
            Assert.Pass();
           


        }
        
        public override void  Testme(string services)
        {
            Console.WriteLine("test method in derived class/child class");
            base.Testme(services);
        }


    }

    public class Test2
    {

        public virtual void Testme(string services)
        {
            Console.WriteLine("testing base class");
        }
    }
}