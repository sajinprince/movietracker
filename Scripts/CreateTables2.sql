﻿Create Database IMDB;
use IMDB;

CREATE TABLE users (
    Id int IDENTITY(1,1) PRIMARY KEY,
    Username varchar(60) NOT NULL,
    Password varchar(60),
);
--DROP TABLE user_episodes;
CREATE TABLE user_episodes(
    EpisodeId int IDENTITY(1,1) PRIMARY KEY,
    SeriesId  int,
    UserId int,
    Watched BIT,
    EpisodeName varchar(60),
);

Insert Into users(Username,Password)
values('derivco','derivco')
Insert Into users(Username,Password)
values('derivco1','derivco1')

--DROP Table episodes;
Create Table episodes(
Id int IDENTITY(1,1) PRIMARY KEY ,
FullName varchar(60),
SeriesId int
)


Create Table series(
Id int IDENTITY(1,1) PRIMARY KEY,
FullName varchar(60),
ImdbId varchar(30),
Category varchar(10),
Released date,
Actors varchar(60),
ImdbRank int
-- NumberOfEpisodes int,
)

ALTER TABLE series add ImageUrl varchar(200)


-- Select * from series inner join userId on user_episodes.UserId=series

--select * from user_episodes inner join series on user_episodes.SeriesId=series.Id
--where UserId=1
/*
Drop table Users
CREATE TABLE users (
    Id uniqueidentifier primary key default newid(),
    Username varchar(60) NOT NULL,
    Password varchar(60),
    --PRIMARY KEY (Id)
);
DROP TABLE user_episodes;
CREATE TABLE user_episodes(
    episode_id varchar(60),
    movie_id  uniqueidentifier,
    user_id uniqueidentifier,
    watched BIT,
);
*/
--Drop table Users;