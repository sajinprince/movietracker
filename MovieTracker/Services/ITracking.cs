﻿using MovieTracker.Models;

namespace MovieTracker.Services
{
    public interface ITracking
    {
        public List<SeriesModel> AddShow(SeriesAddRequest request);
        public List<SeriesModel> AddEpisodes(SeriesAddRequest request, int id);
        public List<SeriesModel> GetSeriesList(int UserId);
        public List<SeriesModel> CheckDuplicateSeries(SeriesAddRequest request);
        public bool CheckDuplicateEpisodes (SeriesAddRequest request);
        public bool UpdateUserEpisodes(EpisodesUpdateRequest request);
        public int GetUserId(CustomLoginModel request);
        public bool DeleteEpisodes(EpisodesUpdateRequest request);
    }
}
