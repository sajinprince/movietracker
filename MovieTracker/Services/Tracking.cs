﻿using Dapper;
using MovieTracker.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using System.Data;
using System.Reflection.Metadata;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace MovieTracker.Services
{
    public class Tracking : ITracking
    {
        private IRepository _repo;
        public Tracking(IRepository repo)
        {
            _repo = repo;
        }

        public List<SeriesModel> GetSeriesList(int UserId) //Service layer has Concrete Models and re-uses generics in the repo layer
        {
            var strSQL = $"select *,UserId from series inner join(select UserId, SeriesId from user_episodes group by UserId, SeriesId) ue on ue.SeriesId = series.Id where UserId = {UserId}";

            var models = _repo.GetList<SeriesModel>(strSQL);

            foreach (var model in models)
            {
                var strSqlChild = $"Select * from user_episodes where SeriesID={model.Id}";
                model.Episodes = _repo.GetList<EpisodesModel>(strSqlChild);
            }

            return models.ToList();
        }

        public List<SeriesModel> CheckDuplicateSeries(SeriesAddRequest request)
        {
            var strSQL = $"select * from user_episodes inner join series on user_episodes.SeriesId = series.Id where UserId = {request.UserId} and FullName='{request.FullName}'";
            var models = _repo.GetList<SeriesModel>(strSQL);

            return models.ToList();
        }

        public bool CheckDuplicateEpisodes(SeriesAddRequest request)
        {
            string episodeNames = "";

            foreach (var strEpisode in request.Episodes) {
                episodeNames = episodeNames + "," + strEpisode.EpisodeName;
            }

            episodeNames = episodeNames.TrimStart(',');
            var strSQL = $"select * from user_episodes inner join series on user_episodes.SeriesId = series.Id where UserId = {request.UserId} and FullName='{request.FullName}' and EpisodeName in ('{episodeNames}')";
            var models = _repo.GetList<SeriesModel>(strSQL);
            var res = (models.Count() > 0) ? true : false;

            return res;
        }

        public List<SeriesModel> AddShow(SeriesAddRequest request)
        {

            var strSQL = "INSERT INTO series (FullName,ImdbId,ImdbRank,ImageURL) VALUES (@FullName, @ImdbId, @ImdbRank,@ImageURL)" +
            "SELECT CAST(SCOPE_IDENTITY() as int)";

            var parameters = new DynamicParameters();
            parameters.Add("ImageURL", request.ImageURL, DbType.String);
            parameters.Add("FullName", request.FullName, DbType.String);
            parameters.Add("ImdbId", request.ImdbId, DbType.String);
            parameters.Add("ImdbRank", request.ImdbRank, DbType.String);
            var id = _repo.UpdateList(strSQL, parameters);

            return AddEpisodes(request, id);
        }

        public List<SeriesModel> AddEpisodes(SeriesAddRequest request, int id)
        {
            foreach (var model in request.Episodes) 
            {
                model.SeriesId = id;
                var parameters2 = new DynamicParameters();
                parameters2.Add("SeriesId", model.SeriesId, DbType.Int32);
                parameters2.Add("UserId", request.UserId, DbType.Int32);
                parameters2.Add("Watched", model.Watched, DbType.Boolean);
                parameters2.Add("EpisodeName", model.EpisodeName, DbType.String);

                var sql = "INSERT INTO user_episodes (SeriesId, UserId,Watched,EpisodeName) VALUES (@SeriesId,@UserId, @Watched,@EpisodeName)" +
                "SELECT CAST(SCOPE_IDENTITY() as int)";

                _repo.UpdateList(sql, parameters2);              
            }

            return GetSeriesList(request.UserId);
        }

        public bool UpdateUserEpisodes(EpisodesUpdateRequest request)
        {
            int id = 0;
            foreach (var model in request.Episodes)
            {
                var sql = "UPDATE user_episodes SET Watched = @Watched WHERE EpisodeId = @EpisodeId and UserId=@UserId ";
                var parameters = new DynamicParameters();

                parameters.Add("EpisodeId", model.EpisodeId, DbType.Int32);
                parameters.Add("Watched", model.Watched, DbType.Boolean);
                parameters.Add("UserId", request.UserId, DbType.Int32);
                
                id = _repo.UpdateRecord(sql, parameters);
            }

            return id > 0;
        }
        public bool DeleteEpisodes(EpisodesUpdateRequest request)
        {
            foreach (var model in request.Episodes)
            {
                var sql = "DELETE from user_episodes WHERE SeriesId = @SeriesId and UserId=@UserId ";
                var parameters = new DynamicParameters();
                parameters.Add("SeriesId", model.SeriesId, DbType.Int32);
                parameters.Add("UserId", request.UserId, DbType.Int32);
                _repo.DeleteRecord(sql, parameters);
                
                var sql2 = "DELETE from series WHERE Id = @SeriesId ";
                var parameters2 = new DynamicParameters();
                parameters2.Add("SeriesId", model.SeriesId, DbType.Int32);
                _repo.DeleteRecord(sql2, parameters2);
            }

            return true;
        }

        public int GetUserId(CustomLoginModel request)
        {
            var strSqlChild = $"Select * from users where Username='{request.Username}' and Password='{request.Password}'";
            var model = _repo.GetList<CustomLoginModel>(strSqlChild);
            var res =  model?.FirstOrDefault()?.Id ??  0 ;

            return res;
        }
    }
}
