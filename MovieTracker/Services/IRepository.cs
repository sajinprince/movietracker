﻿using Dapper;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MovieTracker.Services
{
    public interface IRepository //Using an interface everywhere so we can swap out implemenation if needed
    {
        public IEnumerable<T> GetList<T>(string sql); 
        //public Task<bool> UpdateList<T>(string sql, DynamicParameters parameters);
        public int UpdateList(string sql, DynamicParameters parameters);
        public int UpdateRecord(string sql, DynamicParameters parameters);
        public void DeleteRecord(string sql, DynamicParameters parameters);

    }
}
