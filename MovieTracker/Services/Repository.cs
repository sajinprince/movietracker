﻿namespace MovieTracker.Services { 
    using Dapper;
    using Microsoft.Data.SqlClient;

    public class Repository: IRepository //NB Using generics here to avoid duplicating this code in the service layer
    {
        private string _connectionString;
        public Repository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public IEnumerable<T> GetList<T>(string sql)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var res = connection.Query<T>(sql); 
                return res;  
            }
        }
       
        public int UpdateList(string sql, DynamicParameters parameters)

        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var id=connection.QuerySingle<int>(sql, parameters);
                return id;
            }
        }
        public int UpdateRecord(string sql, DynamicParameters parameters)

        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var id = connection.Execute(sql, parameters);
                return id;
            }
        }
        public void DeleteRecord(string sql, DynamicParameters parameters)

        {
            using (var connection = new SqlConnection(_connectionString))
            {
               
                connection.Execute(sql, parameters);
            }
        }


        /* toddo change to asynch
        // Task.Factory.StartNew(() => connection.Execute(sql, parameters));
       public async Task<int> UpdateList(string sql, DynamicParameters parameters)

       {
           using (var connection = new SqlConnection(_connectionString))
           {
               //var res= connection.ExecuteAsync(sql, parameters).IsCompleted;
               //await connection.Execute(sql, parameters);
               //return res;

               await connection.ExecuteAsync(sql, parameters);

               var id = await connection.QuerySingleAsync<int>(sql, parameters);
               return id;
           }
       }
       */
    }
}
