﻿namespace MovieTracker.Models
{
    public class EpisodesUpdateRequest
    {
        public EpisodesUpdateRequest()
        {
            Episodes = new List<EpisodesModel>();
        }
        public int UserId { get; set; }
        public List<EpisodesModel> Episodes { get; set; }
    }

}
