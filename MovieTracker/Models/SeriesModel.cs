﻿namespace MovieTracker.Models
{
    public class SeriesModel
    {
        public SeriesModel()
        {
            Episodes = new List<EpisodesModel>();
        }
        public IEnumerable<EpisodesModel> Episodes { get; set; }
        public int Id { get; set; }
        public int UserId { get; set; }
        public string? FullName { get; set; }
        public string? ImdbId { get; set; }
        public string? Category { get; set; }
        public string? Actors { get; set; }
        public int ImdbRank { get; set; }
        public string ?ImageURL { get; set; }

    }
}
