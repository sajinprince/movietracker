﻿namespace MovieTracker.Models
{
    public class SeriesAddRequest : SeriesModel
    {
        public int TotalEpisodes { get; set; }
    }
}
