﻿namespace MovieTracker.Models
{
    public class EpisodesModel
    {
        public string? EpisodeName { get; set; }
        public int EpisodeId { get; set; }
        public int SeriesId { get; set; }
        public bool Watched { get; set; }

    }
}
