﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieTracker.Models;
using MovieTracker.Services;

namespace MovieTracker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TrackerController : ControllerBase
    {
        private ITracking _service;
        public TrackerController(ITracking service)
        {
            _service = service;
        }

        [HttpGet("list/{userId}")]
        //[Authorize]
        public List<SeriesModel> Get(int userId)
        {
            return _service.GetSeriesList(userId);
        }

        [HttpPost("add")]
        public IActionResult Post([FromBody] Models.SeriesAddRequest req)
        {
            try
            {
                if (_service.CheckDuplicateEpisodes(req))
                {
                    return BadRequest("Duplicate Series or Episode Name");
                }
                if (_service.CheckDuplicateSeries(req).Count() < 1)
                {
                    _service.AddShow(req);
                }
                else
                {
                    _service.AddEpisodes(req, req.Id);
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
  
        }
        [HttpPost("update")]
        public IActionResult Post([FromBody] Models.EpisodesUpdateRequest req)
        {
            try
            {
                var res = _service.UpdateUserEpisodes(req);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            
            
           
        }

        [HttpDelete("delete/{id}/{userid}/{seriesid}")]
        public void Delete(int id,int userid,int seriesid)
        {           
            var model = new List<EpisodesModel> { 
                new EpisodesModel { 
                    EpisodeId = id, SeriesId = seriesid 
                } 
            };
            var req = new EpisodesUpdateRequest
            {
                UserId = userid,
                Episodes = model
            };
            try
            {
                _service.DeleteEpisodes(req);        
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }

    }
}
