﻿using Microsoft.AspNetCore.Mvc;
using MovieTracker.Models;
using MovieTracker.Services;
using System.Diagnostics;


namespace MovieTracker.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private ITracking _service;

        public HomeController(ILogger<HomeController> logger, ITracking service)
        {
            _logger = logger;
            _service = service;
        }

        public IActionResult Index()
        {
  
            return View(_service.GetSeriesList(1));
        }
        [HttpPost]
        public IActionResult AuthenticateUser(CustomLoginModel model)
        {
            
            var useId = _service.GetUserId(model);
            if (useId == 0)
            {
                var userModel = new CustomLoginModel { Email = "", Username = "InvalidLogin" };
                return View("Login", userModel);
            }
            return View("Index",_service.GetSeriesList(useId));
        }

        
        public IActionResult Login()
        {
         
            var userModel = new CustomLoginModel { Email = "", Username = "derivCo" };
            return View("Login", userModel);
        }

        public IActionResult Add(int userId)
        {
            if (userId == 0)
            {
                userId = 1; //Default for DemoUser
            }
            var model = new SeriesAddRequest { UserId=userId };
            return View("Add", model);
        }
        [HttpPost]
        public IActionResult CreateSeries(SeriesAddRequest model)
        {
           
            var episodes = new List<EpisodesModel>();
            for (int ix=0; ix <model.TotalEpisodes;ix++ )
            {
                episodes.Add(new EpisodesModel
                {
                    EpisodeId = ix,
                    EpisodeName = $"Episode{ix}",
                    Watched=false,

                }); ;
            }
            model.Episodes = episodes;
            var check = _service.CheckDuplicateSeries(model);
            if (check==null ||check.Count() < 1)
            {
                _service.AddShow(model);
            }
            else
            {
                _service.AddEpisodes(model, check?.FirstOrDefault()?.Id ?? 0);
            }

            return View("Index", _service.GetSeriesList(model.UserId));            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpPost]
        public IActionResult Update(Models.SeriesModel model)
        {

            var UserId = Convert.ToInt32(Request.Form["UserId"]);
            var mappedEpisodes = new List<EpisodesModel>();
            var iy = 0;

            foreach (string item in Request.Form["EpsisodeID"])
            {
                if (Request.Form["Watched"].ToList().Count()>=iy)
                {
                   
                    mappedEpisodes.Add(new EpisodesModel
                    {
                        EpisodeId = Convert.ToInt32(item),
                        Watched= Convert.ToBoolean(Request.Form["Watched"][iy])

                    });
                }
                iy++;
            }

            var apiRequest = new EpisodesUpdateRequest
            {
                UserId = UserId,
                Episodes= mappedEpisodes,
                
            };
            _service.UpdateUserEpisodes(apiRequest);
            
            return View("Index", _service.GetSeriesList(UserId));
        }

        public IActionResult Delete(int id, int seriesid)
        {
            var model = new List<EpisodesModel> {
                new EpisodesModel {
                    EpisodeId = 0, SeriesId = seriesid
                }
            };
            var req = new EpisodesUpdateRequest
            {
                UserId = id,
                Episodes = model
            };
            _service.DeleteEpisodes(req);

            return View("Index", _service.GetSeriesList(id));
        }
    }
}