using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
using MovieTracker.Data;
using MovieTracker.Services;

var builder = WebApplication.CreateBuilder(args);
string connectionString = builder.Configuration.GetConnectionString("AuthConnectionString") ?? throw new InvalidOperationException("Connection string 'MovieTrackerContextConnection' not found.");

builder.Services.AddDbContext<MovieTrackerContext>(options =>
    options.UseSqlServer(connectionString));

builder.Services.AddDefaultIdentity<MovieTracker.Areas.Identity.Data.MovieTrackerUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<MovieTrackerContext>();

// Injecting services into the container Denpendency Inversion Principal.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllersWithViews();
builder.Services.AddSingleton<IRepository>(new Repository(connectionString));
builder.Services.AddScoped<ITracking, Tracking>();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}

app.UseSwagger();
app.UseSwaggerUI();
app.UseStaticFiles();
app.UseRouting();
app.UseAuthentication();;
app.UseAuthorization();
app.MapControllerRoute(name: "default",pattern: "{controller=Home}/{action=Login}/{id?}");

app.Run();

